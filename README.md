# README

This is a simple notes app

* Ruby version

Use ruby 2.5.0, if you have a ruby version manager like rvm, run:

```
rvm use 2.5.0
```

* Configuration

Once you have the repo cloned to your computer, install all the dependencies by running:

```
bundle install
```

* Database initialization

To setup the db, use Postgres.app if you are in a Mac and run:

```
rails db:setup
```


* Deployment instructions

To deploy simply run:

```
git push heroku master
```
And don't forget to run migrations:

```
heroku run rake db:migrate
```